from logging.handlers import RotatingFileHandler


class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age

  def myfunc(self):
    print("Hello my name is " + self.name)
    print("My age is ", self.age)

p1 = Person("John", 36)
p1.myfunc()

p1.age = 40
p1.name = "Rafael"
p1.myfunc()

p2 = Person("Thor", 43)
p2.myfunc()