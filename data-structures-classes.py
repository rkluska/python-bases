# Classes: Code base for related code
# ------------------------------

from mailbox import NotEmptyError


class Carros:
    def __init__(self):
        print('Carros instanciada')

Carros()

class Carros:
    def __init__(self, nome, cor, ano):
        self.nome = nome
        self.cor = cor
        self.ano = ano
        print("Um carro foi criado")

    def metodo(self):
        print("metodo executado")

Carro1 = Carros("C 180", "Preta", "2019")
print(Carro1)

exit()




cars = ["Ford", "Volvo", "BMW"]
numbers = [[1, 2, 3], [1, 2, 3], [1, 3, 2]]

graph = {
    1: [2, 3, cars],
    2: [4, cars],
    3: [5, 6, cars]
}

cars.append("toyota")
print(cars)

cars.insert(0, "toyota")
print(cars)

cars.pop(0)
print(cars)

cars.pop(len(cars)-1)
print(cars)

