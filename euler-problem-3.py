# Problem 3 - Largest prime factor
# ___________________________________________________________________________________________________________________________________
print("The prime factors of 13195 are 5, 7, 13 and 29.")
print("What is the largest prime factor of the number 600851475143 ?")

# 1st step
# Search in web how to calculate prime factor

# print(10 % 2)
# print(9 % 3)
# print(8 % 4)
# print(7 % 2)
# print(7 % 3)
# print(5 % 2)
# print(5 % 3)
# print(2 % 2)
# print(2 % 3)
# exit()

# Solution
# Check number by number is an prime factor
def isPrime(x):
    # invalid input
    if x <= 1:
        return False

    # process all potential divisors
    for i in range(2,x):
        if x % i == 0:
            return False 
    # no divisor found, therfore it's a prime number
    return True

# the number to we want largest prime factor of
number = int(input("What is the largest prime factor of the number: "))
# ------------------------------
#  TRACK TIME - START
# ------------------------------
# using time module
import time
# ts stores the time in seconds
ts0 = time.time()
# ----------------------------
# a list to store all the prime factors of a number
Factors=[]
Factors.append(1)
# iterate from 2 to half of the number as there can be no factor
# greater than half of the number.
for i in range(2, number//2 + 1):
    # check if number is a factor
    if number % i == 0:
        # check if factor is also a prime
        if isPrime(i)==True:
            # add the number to the list
            Factors.append(i)
# output the maximum of the factors to obtain largest prime factor
print("Largest Prime Factor =",max(Factors))

       
# ------------------------------
#  TRACK TIME - END
# ------------------------------
print("------------------------------------------------------------------------------------------")
print("Score for execution: ", i)
ts1 = time.time()
print("Execution time: ",(ts1 - ts0),"seconds")
print("Execution time: ",(ts1 - ts0)/60,"minuties")