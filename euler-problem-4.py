# Problem 4 - prime factor
# ___________________________________________________________________________________________________________________________________
print("Print all primes of a number")

# 1st step
# Search in web how to calculate prime factor

# print(10 % 2)
# print(9 % 3)
# print(8 % 4)
# print(7 % 2)
# print(7 % 3)
# print(5 % 2)
# print(5 % 3)
# print(2 % 2)
# print(2 % 3)
# exit()

# Solution
# Check number by number is an prime factor

class numbers:
    def __init__(self, number):
        self.number = number
    
    def check_prime(n):
        if((n % 2) == 0):
            return False;
        elif ((n % 3) == 0):
            return False;
        else:
            return True;

    def check_prime_fac(self, nb):
        if((self.number % nb) == 0):
            print (self.number,"%",nb,"=",(self.number % nb))
            return True;
        else:
            print (self.number,"%",nb,"=",(self.number % nb))
            return False;
    
class tracktime:
    def __init__(self):
        import time
        self.start = time.time()
        self.fishi= time.time()
   
    def duration(self):
        self.duratione = self.start - self.fishi
    
    def print_duration_seconds(self):
        print("Duration:", self.duratione, "Seconds")

    def print_duration_minuties(self):
        print("Duration:", self.duratione/60, "Minuties")

class theme1:
    def div():
        print("------------------------------------------------------------------------------------------")

nb0 = numbers(int(input("Input a number: ")))
# ------------------------------
#  TRACK TIME - START
# ------------------------------
# using time module
# t stores the time in seconds
t = tracktime()
# ----------------------------
wi = 0
pf = False
nb = int(nb0.number / 2)
print("start!")

while(pf == False):
    print(nb)
    if(numbers.check_prime(nb)):
        print("---> Is a Prime: ", nb)
        #check is a prime factory
        if(numbers.check_prime_fac(nb0, nb)):
            pf = True
            print("------->Prime factor: ", nb)
    wi += 1
    nb -= 1
print("the largest prime factor of",nb0.number,"is", (nb + 1))
# ------------------------------
#  TRACK TIME - END
# ------------------------------
theme1.div()
print("Score for execution: ", wi)
tracktime.end(t)
tracktime.duration(t)
tracktime.print_duration_seconds(t)