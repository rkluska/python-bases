cars = ["Ford", "Volvo", "BMW"]
numbers = [[1, 2, 3], [1, 2, 3], [1, 3, 2]]

graph = {
    1: [2, 3, cars],
    2: [4, cars],
    3: [5, 6, cars]
}

cars.append("toyota")
print(cars)

cars.insert(0, "toyota")
print(cars)

cars.pop(0)
print(cars)

cars.pop(len(cars)-1)
print(cars)

