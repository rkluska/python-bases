class Files:
    def __init__(self,file_path):
        with open(file_path, 'r') as self.file:
            self.file.names = self.file.name
            self.file.contents = self.file.read()
            self.file.close()

    def contents(self):
        print(self.contents)

    def name2(self):
        print(self.names)

file1 = Files('file-example-22.txt')
print(file1.name)

# with open('file-example-22.txt', 'r') as file:
    # file_content = file.read()
    # print(file.name)
# print(file.read())
